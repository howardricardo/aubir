# Aubir

1. Adicione sua foto com nome `person_image.jpeg` no diretório `assets`
2. Mude os parâmetros `birth_date` e `name_text` no arquivo `main.py`
3. Execute o arquivo `main.py`
4. Veja a imagem `new_image.jpg` do diretório `assets`.
