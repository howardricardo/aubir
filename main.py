from PIL import Image, ImageDraw, ImageFont, ImageFilter
from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent
ASSETS_DIR = BASE_DIR / "aubir/assets/"
DEFAULT_FONT = "assets/waltham-regulardb.ttf"
DEFAULT_FILES_NAMES = {
    "base": "base_image.jpg",
    "new": "new_image.jpg",
    "person": "person_image.jpeg",
    "mask": "mask_image.jpg",
}

base_img = Image.open(ASSETS_DIR / DEFAULT_FILES_NAMES["base"])

draw = ImageDraw.Draw(base_img)

birth_date = "11/03"  # Deve ser dinâmico
birth_date_text_orientation = (260, 310)
font_birth_date = ImageFont.truetype(DEFAULT_FONT, 90)

draw.text(birth_date_text_orientation, birth_date, fill=(0, 0, 0), font=font_birth_date)


# Tudo isso aqui abaixo apenas para dar um leve "rotate" no texto do nome.
name_text = "Howard Medeiros"  # Deve ser dinâmico
name_text_orientation = (180, 870)
font_name = ImageFont.truetype(DEFAULT_FONT, 50)
name_image = Image.new("L", (380, 70), 255)
name_draw = ImageDraw.Draw(name_image)
name_draw.text((0, 0), name_text.upper(), font=font_name)
name_rotated = name_image.rotate(3, expand=False, fillcolor="white")

base_img.paste(name_rotated, name_text_orientation)

person_image = Image.open(
    ASSETS_DIR / DEFAULT_FILES_NAMES["person"]
)  # Arquivo com esse nome, mas a imagem deve ser dinâmica
base_width = 350
wpercent = base_width / float(person_image.size[0])
hsize = int((float(person_image.size[1]) * float(wpercent)))
person_image = person_image.resize((base_width, hsize))
person_image.save(ASSETS_DIR / DEFAULT_FILES_NAMES["person"], quality=100)

mask_image = Image.new("L", person_image.size)
draw = ImageDraw.Draw(mask_image)
draw.ellipse((10, 10, 350, hsize), fill=255)

mask_image_blur = mask_image.filter(ImageFilter.GaussianBlur(15))
mask_image_blur.save(ASSETS_DIR / DEFAULT_FILES_NAMES["mask"], quality=100)

person_image_orientation = (180, 450)

base_img.paste(person_image, person_image_orientation, mask_image_blur)

base_img.save(ASSETS_DIR / DEFAULT_FILES_NAMES["new"], quality=100)
